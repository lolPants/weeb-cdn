# Weeb CDN
Automatic CDN for Weeb Stuff.
Built by [Jack Baron](https://www.jackbaron.com)


ROUTES:

STATIC
/cdn        Serves all static files.
            API Requests will point to an image in this route.
API
/api/v1.0   Base Route for all API Requests


API ENDPOINTS:

GET /:dir
Lists the content in a directory.
Returns a JSON file with the maximum index and an array of files[]

GET /:dir/:index
Returns a JSON object describing a file. This JSON object is used in all
routes describing a file.

{ index: int, url: str, ext: str, mime: str }
Where:  index - Integer index of the file used in the API
        url   - URL of the file in the static CDN route
        ext   - File extension of the requested item
        mime  - MIME type of the requested item

GET /:dir/random
Returns a JSON object describing a random file from a directory.

GET /:dir/randimg
Returns a random actual image.
(USAGE OF THIS IS NOT RECOMMENDED AS IT IT PROXIED THROUGH THE API)
