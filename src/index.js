// Package Dependencies
const path = require('path')
const fs = require('fs-extra')
const express = require('express')
const cors = require('cors')

// Express App
const app = express()
app.set('trust proxy')
app.use(cors())
app.use((req, res, next) => {
  res.set('X-Docker-Hostname', process.env.HOSTNAME)
  res.removeHeader('X-Powered-By')
  next()
})

// Public Directory Constant
const PUBLIC_DIR = path.join(__dirname, 'public')

// Give information about a directory
app.get('/api/v1.0/:dir', async (req, res) => {
  let { dir } = req.params

  let dirPath = path.join(PUBLIC_DIR, dir)
  if (!await fs.exists(dirPath)) return res.sendStatus(404)

  let files = (await fs.readdir(dirPath))
    .map(x => `${req.secure ? 'https://' : 'http://'}${req.headers.host}/cdn/${req.params.dir}/${encodeURIComponent(x)}`)
  let max = files.length - 1
  res.send({ max, files })
})

// Fetch a random image from a directory and serve info
app.get('/api/v1.0/:dir/random', async (req, res) => {
  let { dir } = req.params

  let dirPath = path.join(PUBLIC_DIR, dir)
  if (!await fs.exists(dirPath)) return res.sendStatus(404)

  let files = (await fs.readdir(dirPath))
    .map(x => `${req.secure ? 'https://' : 'http://'}${req.headers.host}/cdn/${req.params.dir}/${encodeURIComponent(x)}`)

  let index = Math.floor(Math.random() * files.length)
  let url = files[index]
  let { ext } = path.parse(url)
  res.send({ index, url, ext })
})

// Fetch a random image from a directory and serve image
// Not Recommended for use
app.get('/api/v1.0/:dir/randimg', async (req, res) => {
  let { dir } = req.params

  let dirPath = path.join(PUBLIC_DIR, dir)
  if (!await fs.exists(dirPath)) return res.sendStatus(404)

  let files = await fs.readdir(dirPath)
  let file = path.join(dirPath, files[Math.floor(Math.random() * files.length)])

  res.sendFile(file)
})

// Fetch a specific image from a directory and serve
app.get('/api/v1.0/:dir/:index', async (req, res) => {
  let { dir, index } = req.params
  index = parseInt(index)

  let dirPath = path.join(PUBLIC_DIR, dir)
  if (!await fs.exists(dirPath)) return res.sendStatus(404)

  let files = (await fs.readdir(dirPath))
    .map(x => `${req.secure ? 'https://' : 'http://'}${req.headers.host}/cdn/${req.params.dir}/${encodeURIComponent(x)}`)

  let url = files[index]
  if (!url) return res.sendStatus(404)

  let { ext } = path.parse(url)
  res.send({ index, url, ext })
})

// Listen for connections
app.listen(3000)
