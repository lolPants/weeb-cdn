# Weeb CDN ![](https://gitlab.com/lolPants/weeb-cdn/badges/master/build.svg)
_Automatic CDN for Weeb Stuff._  
Built by [Jack Baron](https://www.jackbaron.com)

## Maintainers
- lolPants (https://www.jackbaron.com)
